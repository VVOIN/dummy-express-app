'use strict';

const application = require('./src/application');
const createServer = require('./src/createServer');
const connectDB = require('./src/connectDB');
const config = require('./config/config');

const app = application();
const server = createServer(app);

connectDB(config.mongoURI, {}, err => {
	if (err) throw new Error('unable to connect to DB', err);
	server.listen({
		host: config.host,
		port: config.port
	}, () => console.info('listening...'));
});

