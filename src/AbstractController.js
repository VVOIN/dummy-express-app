'use strict';

/**
 * @interface
 */
class AbstractController {
	/**
	 * Passes express Router object while initialization
	 * @param {Object<express.Router>} router
	 * @param {Function} check
	 * @param {Function} sanitize
	 * @abstract
	 * @interface
	 * @return Void
	 */
	addRoutes(router, check, sanitize) {
		if (!router) throw new Error('router is not defined');
		if (!check) throw new Error('check is not defined');
		if (!sanitize) throw new Error('sanitize is not defined');
		throw new Error('addRoutes is not implemented in AbstractController');
	}
}

module.exports = AbstractController;

