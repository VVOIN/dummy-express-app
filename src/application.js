'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const { check } = require('express-validator/check');
const { sanitize } = require('express-validator/filter');
const PersonController = require('./person/personController');

module.exports = function () {
	const app = express();

	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());

	app.use(helmet());

	const router = express.Router();
	const personController = new PersonController();
	personController.addRoutes(router, check, sanitize);
	app.use(router);

	return app;
};

