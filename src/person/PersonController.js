'use strict';

const _ = require('lodash');

const AbstractController = require('../AbstractController');
const PersonCollection = require('./PersonCollection');

class PersonController extends AbstractController {
	addRoutes(router, check, sanitize) {
		router.get('/person', [
			check('email').isEmail().withMessage('must be an email'),
			sanitize('name'),
			sanitize('email'),
			sanitize('phone')
		],this.onGetByQuery.bind(this));
		router.get('/person/:id', [
			check('id').exists(),
			sanitize('id')
		], this.onGetByID.bind(this));
		router.post('/person/', [
			check('name').exists(),
			check('email').exists(),
			check('phone').exists(),
			sanitize('name'),
			sanitize('email'),
			sanitize('phone')
		], this.onCreate.bind(this));
	}
	onGetByQuery(req, res) {
		const query = _.pick(req.query, ['name', 'email', 'phone']);
		PersonCollection
			.findBy(query)
			.then(person => res.send(JSON.stringify(person)))
			.catch(err => res.status(500).send(err));
	}
	onGetByID(req, res) {
		const id = req.params.id;
		PersonCollection
			.findByID(id)
			.then(person => res.send(JSON.stringify(person)))
			.catch(err => res.status(500).send(err));
	}
	onCreate(req, res) {
		const name = req.body.name;
		const email = req.body.email;
		const phone = req.body.phone;
		PersonCollection
			.create({ name, email, phone})
			.then(person => res.send(JSON.stringify(person)))
			.catch(err => res.status(500).send(err));
	}
}

module.exports = PersonController;