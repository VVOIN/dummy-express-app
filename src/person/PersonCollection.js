'use strict';

const Joi = require('joi');
const MongoModels = require('mongo-models');

class PersonCollection extends MongoModels {
	static create({ name, email, phone }) {
		return new Promise((resolve, reject) =>
			this.insertOne({ name, email, phone }, (err, person) => {
				if (err) return reject(err);
				return resolve(person);
			})
		);
	}

	static findBy(query) {
		return new Promise((resolve, reject) =>
			this.find(query, (err, person) => {
				if (err) return reject(err);
				return resolve(person);
			})
		);
	}

	static findByID(id) {
		return new Promise((resolve, reject) =>
			this.findById(id, (err, person) => {
				if (err) return reject(err);
				return resolve(person);
			})
		);
	}
}

PersonCollection.collection = 'persons';

PersonCollection.schema = Joi.object().keys({
	name: Joi.string().required(),
	email: Joi.string().email(),
	phone: Joi.string()
});

module.exports = PersonCollection;