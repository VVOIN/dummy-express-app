'use strict';

const MongoModels = require('mongo-models');

module.exports = function (mongoURI, options, callback) {
	MongoModels.connect(mongoURI, options, callback);
};